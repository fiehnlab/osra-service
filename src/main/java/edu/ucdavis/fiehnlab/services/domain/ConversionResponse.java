package edu.ucdavis.fiehnlab.services.domain;

/**
 * Created by wohlgemuth on 9/13/16.
 */
public class ConversionResponse {
    private String inchiCode;

    private String inchiKey;

    private String errorLog;

    private String outputLog;

    public ConversionResponse(String inchiCode, String inchiKey, String outputLog, String errorLog) {
        this.inchiCode = inchiCode;
        this.inchiKey = inchiKey;
        this.outputLog = outputLog;
        this.errorLog = errorLog;
    }


    public String getInchiCode() {
        return inchiCode;
    }

    public void setInchiCode(String inchiCode) {
        this.inchiCode = inchiCode;
    }

    public String getInchiKey() {
        return inchiKey;
    }

    public void setInchiKey(String inchiKey) {
        this.inchiKey = inchiKey;
    }

    public String getOutputLog() {
        return outputLog;
    }

    public void setOutputLog(String outputLog) {
        this.outputLog = outputLog;
    }

    public String getErrorLog() {
        return errorLog;
    }

    public void setErrorLog(String errorLog) {
        this.errorLog = errorLog;
    }

}
