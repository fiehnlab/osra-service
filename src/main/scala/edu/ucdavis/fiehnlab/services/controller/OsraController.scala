package edu.ucdavis.fiehnlab.services.controller

import java.io.{BufferedOutputStream, File, FileOutputStream}
import javax.servlet.http.HttpServletResponse

import edu.ucdavis.fiehnlab.services.domain.ConversionResponse
import edu.ucdavis.fiehnlab.services.services.OsraService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod, RequestParam, RestController}
import org.springframework.web.multipart.MultipartFile

/**
  * Created by wohlg on 9/9/2016.
  */
@RestController
@RequestMapping(Array("/"))
class OsraController {

  val temp = File.createTempFile("temp","temp").getParentFile

  @Autowired
  val osraService:OsraService = null

  @RequestMapping(value=Array("/convert"), method=Array(RequestMethod.POST))
  def handleConversion(@RequestParam(value = "file")file:MultipartFile):Array[ConversionResponse] = {
      //save the file
      val content = file.getBytes
      val outFile = new File(temp,file.getOriginalFilename)

      val out = new BufferedOutputStream(new FileOutputStream(outFile))
      out.write(content)
      out.flush()
      out.close()

      //run the osra converter
      try {
        osraService.convert(outFile)
      }
      finally {
        //cleanup
        outFile.delete()
      }
  }

  @RequestMapping(value = Array("/"), method = Array(RequestMethod.GET))
  def index(response:HttpServletResponse) = {
    response.sendRedirect("https://bitbucket.org/fiehnlab/osra-service/overview")
  }
}
