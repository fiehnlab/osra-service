package edu.ucdavis.fiehnlab.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OsraApplication {

    public static void main(String[] args) {
        SpringApplication.run(OsraApplication.class, args);
    }
}
