# README #

This project allows you to easily access the [osra](https://cactus.nci.nih.gov/osra/) tool in form of a simple web service to allow the converting of image files to structures.

We are not related or provided funding for this tool and only provide a simple REST based wrapper, to utilize it from different platforms, without installation requirements.

## How do I get set up? ##

### Running the docker image ###

	docker run -it -p 8080:8080 eros.fiehnlab.ucdavis.edu/mona-osra

### Running the docker image under openshift ###

 	oc new-app eros.fiehnlab.ucdavis.edu/mona-osra --name osra
 	oc expose service osra --hostname=struct.fiehnlab.ucdavis.edu
	
## Compiling the source code ##

	mvn clean install

## Using the service ##

curl -F file=@imagefly.png localhost:8080/convert

### Service response ###

The service will respond with a JSON Array, containing objects with the following fields

1. InChI Code: the discovered InChI code or null
2. InChI Key: the discovered InChI key
3. outputLog: the complete sdf and logfile produced by osra
4. errorLog: any potentially observed errors by osra


### Accessing our production system ###

curl -F file=@imagefly.png http://struct.fiehnlab.ucdavis.edu/convert