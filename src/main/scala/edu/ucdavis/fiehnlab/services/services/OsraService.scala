package edu.ucdavis.fiehnlab.services.services

import java.io.{ByteArrayInputStream, File}

import edu.ucdavis.fiehnlab.services.domain.ConversionResponse
import org.openscience.cdk.{ChemFile, DefaultChemObjectBuilder}
import org.openscience.cdk.io.MDLV2000Reader
import org.openscience.cdk.io.iterator.IteratingSDFReader
import org.openscience.cdk.tools.manipulator.ChemFileManipulator
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

import scala.collection.JavaConverters._
import scala.io.Source

/**
  * Created by wohlg on 9/9/2016.
  */
@Service
class OsraService {

  val logger = LoggerFactory.getLogger(getClass)

  /**
    * converts the given file and returns our response object
    *
    * @param file
    * @return
    */
  def convert(file: File): Array[ConversionResponse] = {

    logger.info(s"${file} exists ${file.exists()}")
    val osraFlags = "-f sdf --embedded-format inchi"

    val systemCommand: Array[String] = System.getProperty("os.name").toLowerCase() match {
      case x if x.contains("linux") =>
        Array("/bin/bash", "-c", "-l", s"/usr/local/bin/osra ${osraFlags} ${file.getAbsolutePath}")
      case x: String => throw new RuntimeException(s"${x} not yet supported")
    }

    logger.info(s"executing command: ${systemCommand.mkString(" ")}")

    val process = Runtime.getRuntime.exec(systemCommand)
    process.waitFor()
    val results = Source.fromInputStream(process.getInputStream).getLines().toList
    val errors = Source.fromInputStream(process.getErrorStream).getLines().toList

    logger.info(s"read lines: ${results.size}")

    if (results.isEmpty) {
      Array[ConversionResponse]()
    }
    else {

      val response = results.mkString("\n")

      val reader = new MDLV2000Reader(new ByteArrayInputStream(response.getBytes("UTF-8")))

      logger.info(s"read errors: ${errors.size}")
      errors.foreach(logger.info)

      ChemFileManipulator.getAllAtomContainers(reader.read(new ChemFile())).asScala.map { model =>
        new ConversionResponse(model.getProperty("inchi"),model.getProperty("inchi_key"),response,errors.mkString("\n"))
      }.toArray
    }
  }
}
